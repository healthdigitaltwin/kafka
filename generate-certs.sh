#!/bin/bash

# Set the certs directory
CERTS_DIR="./certs"

# Generate CA Key if not present
if [ ! -f "$CERTS_DIR/ca.key" ]; then
  openssl genrsa -out $CERTS_DIR/ca.key 2048
  echo "CA Key generated successfully."
fi

# Generate CA Certificate if not present
if [ ! -f "$CERTS_DIR/ca.crt" ]; then
  openssl req -new -x509 -sha256 -days 365 -key $CERTS_DIR/ca.key -out $CERTS_DIR/ca.crt -subj "/CN=Your CA Name"
  echo "CA Certificate generated successfully."
fi

# Generate SSL Keystore
keytool -genkeypair -alias kafka -keyalg RSA -keysize 2048 -validity 365 -keystore $CERTS_DIR/kafka.keystore.jks -storepass password -keypass password -dname "CN=localhost" -ext SAN=dns:localhost,ip:127.0.0.1

# Generate SSL Truststore
keytool -keystore $CERTS_DIR/kafka.truststore.jks -alias CARoot -import -file $CERTS_DIR/ca.crt -storepass password -keypass password -noprompt

# Generate SSL Key Pair
openssl req -new -newkey rsa:2048 -nodes -keyout $CERTS_DIR/ssl.key -out $CERTS_DIR/ssl.csr -subj "/CN=localhost"
openssl x509 -req -CA $CERTS_DIR/ca.crt -CAkey $CERTS_DIR/ca.key -in $CERTS_DIR/ssl.csr -out $CERTS_DIR/ssl.crt -days 365 -CAcreateserial

# Create SSL Key Credentials File
echo "ssl.key.password=password" > $CERTS_DIR/ssl_key_credentials.txt
echo "ssl.key.alias=kafka" >> $CERTS_DIR/ssl_key_credentials.txt
echo "ssl.key.location=$CERTS_DIR/ssl.key" >> $CERTS_DIR/ssl_key_credentials.txt

# Clean up intermediate files
rm $CERTS_DIR/ssl.csr $CERTS_DIR/ca.srl

echo "SSL keystores, truststores, certificates, and ssl_key_credentials.txt generated successfully."




